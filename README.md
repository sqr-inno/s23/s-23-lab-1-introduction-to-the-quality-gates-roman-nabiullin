[![pipeline status](https://gitlab.com/sqr-inno/s23/s-23-lab-1-introduction-to-the-quality-gates-roman-nabiullin/badges/main/pipeline.svg)](https://gitlab.com/sqr-inno/s23/s-23-lab-1-introduction-to-the-quality-gates-roman-nabiullin/-/commits/main)

[![coverage report](https://gitlab.com/sqr-inno/s23/s-23-lab-1-introduction-to-the-quality-gates-roman-nabiullin/badges/main/coverage.svg)](https://gitlab.com/sqr-inno/s23/s-23-lab-1-introduction-to-the-quality-gates-roman-nabiullin/-/commits/main)

# Lab 1 -- Introduction to the quality gates

Project in deployed using heroku, and should be accessible on [https://sqr-labs.herokuapp.com/hello](https://sqr-labs.herokuapp.com/hello)
